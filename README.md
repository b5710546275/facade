Facade pattern is a pattern that design to manage the system for a client. It provide an interface for the client to interact with it instead of interact with the system itself. This provide a way to reduce a complexity for the client and make it easier to work with. Facade pattern consist of main part: package( a variety of class which can implement an interface), a facade class which responsible for controlling a system and a client which interacts/use with the facade class.




Facade and Factory pattern can be misunderstood from each other. Unlike factory, facade is not responsible for create a object, it responsible for doing a task with the package. Facade can create an object but it doesn’t return an object to a client but it will do some task or call some method of the object. 

Main

    public class Main {
    	public static void main(String[] args) {
    		Facade facade = new Facade():
    		facade.drawEyes();
    	}
    }
    
    
Facade


    public class Facade {
        private list<Shape> lists;
    	    public void drawEyes(){
                list = new ArrayList<Shape>():
    		    Circle circle = new Circle();
    	        circle.draw(0,0,25,25);
                list.add(circle);
                circle = new Circle();
                circle.draw(100,0,125,25);
                list.add(circle);
    }
}

Circle


    public class Circle{
    	public void draw(x1,y1,x2,y2){
    	……..
        }
    }